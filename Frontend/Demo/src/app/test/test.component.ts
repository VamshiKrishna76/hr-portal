import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  
  id: number;
  name: string;
  age: number;
  address: any;
  hobbies: any;

  constructor() {
    this.id=101;
    this.name='Rohith';
    this.age=21;
    this.address={streetNo:13, city:'Linganvai',state:'Telangana'};
    this.hobbies=['Music','Playing','Eating','Sleeping'];
  }

  ngOnInit(){
  }

}
