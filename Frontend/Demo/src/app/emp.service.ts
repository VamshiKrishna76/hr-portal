import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus: Subject<any>;
  isUserLogged: boolean;
  //For Products AddToCart
  cartItems: any;

  constructor(private http:HttpClient) { 
    this.isUserLogged = false;
    this.cartItems = [];
    this.loginStatus = new Subject();
  }

  //Login
  setUserLogIn() {
    this.isUserLogged = true;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus.next(true);
  }
  //Logout
  setUserLogOut() {
    this.isUserLogged = false;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus.next(false);
  }
  //AuthGuard
  getLoginStatuss(): boolean {
    return this.isUserLogged;
  }

   //To Enable and Disable Login, Register and Logout
  getStatusLogin(): any {
    return this.loginStatus.asObservable();
  }

  empLogin(loginForm: any) {
    return this.http.get("empLogin/" + loginForm.emailId + "/" + loginForm.password).toPromise();
  }

  addToCart(product: any) {
    this.cartItems.push(product);
  }
  
  getCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllEmployees(): any {
    return this.http.get("getAllEmployees");
  }

  getAllDepartments(): any {
    return this.http.get('getAllDepartments');
  }

  getEmpById(empId: any): any {
    return this.http.get('getEmpById/' + empId);
  }

  registerEmp(employee: any){
    return this.http.post('registerEmployee', employee);
  }

  updateEmployee(employee: any) {
    return this.http.put('updateEmployee', employee);
  }

  deleteEmployee(empId: any): any {
    return this.http.delete('deleteEmpById/' + empId);
  }
}
