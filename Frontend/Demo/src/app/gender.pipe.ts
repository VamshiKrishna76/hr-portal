import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(empname: any,gender:any): any {
    if(gender=="Male"){
      return "Mr."+empname;
    }
    else if(gender=="Female"){
      return  "Mrs."+empname;
    }
    return empname;
  }

}