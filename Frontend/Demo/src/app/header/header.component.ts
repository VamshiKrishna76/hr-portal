import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  loginStatus: any;

  //For Count of CartItems
  cartItems: any;

  constructor(private service: EmpService) {
    //For Count of CartItems
    this.cartItems = this.service.cartItems;
  }

  ngOnInit() {
    this.service.getStatusLogin().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
}
