import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent {

  cartItems: any;
  total: number;

  //Dependency Injection of EmpService for cartItems
  constructor(private service: EmpService) {
    this.total = 0;

    //Using EmpService for cartItems
    this.cartItems = this.service.cartItems;

    this.cartItems.forEach((product: any) => {
      this.total = this.total + product.price;
    });

  }

  ngOnInit() {
  }

  deleteFromCart(prod: any) {
    const i = this.cartItems.findIndex((product: any) => {
      return product.id == prod.id;
    });

    this.total = this.total - this.cartItems[i].price;
    this.cartItems.splice(i, 1);    
  }

  checkOut() {
    this.total = 0;
    this.cartItems = null;    
  }
}
