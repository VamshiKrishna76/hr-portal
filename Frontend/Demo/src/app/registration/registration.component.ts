import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit{

  employee : any;
  countries: any;
  departments: any;

  constructor(private service: EmpService){
    this.employee ={
      "empId":"",
      "empName":"",
      "salary":"",
      "gender":"",
      "doj":"",
      "country":"",
      "emailId":"",
      "password":"",
      "department":{
        "deptId":""
      }
    }
  }


  ngOnInit() {
    this.service.getCountries().subscribe((data: any) => {
      this.countries = data;
      console.log(data);
    });

    this.service.getAllDepartments().subscribe((data: any) => {
      this.departments = data;
      console.log(data);
    });
}

register() {
  console.log(this.employee);
  
  this.service.registerEmp(this.employee).subscribe((data: any) => {
    console.log(data);
  });
}
}